# Use an official Python runtime as a base image
FROM ubuntu

# Set the working directory to /app
WORKDIR /work

# Copy the current directory contents into the container at /app
ADD . /work

RUN apt-get update && cat requirements_apt.txt | xargs apt-get -y install

# Install any needed packages specified in requirements_pip.txt
RUN pip3 install -r requirements_pip.txt
		    
# get tesseract-ocr french
RUN wget http://ftp.fr.debian.org/debian/pool/main/t/tesseract-fra/tesseract-ocr-fra_3.04.00-1_all.deb

# install french
RUN dpkg -i tesseract-ocr-fra_3.04.00-1_all.deb

# download punkt nltk
RUN python3.5 -m nltk.downloader punkt

# add alias
RUN echo 'alias python="python3.5"' >> ~/.bashrc
#RUN source ~/.bashrc
RUN /bin/bash -c "source ~/.bashrc"


# Set the locale
ENV LANG en_US.UTF-8
ENV LC_NUMERIC fr_FR.UTF-8
ENV LANGUAGE en_US
ENV LC_IDENTIFICATION fr_FR.UTF-8

# Run app.py when the container launches
CMD ls -a
