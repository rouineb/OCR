import os
import sys
import xlrd
from shutil import  rmtree
from extra_decorators import singleton
from document import Document
from settings import ENCODING

@singleton
class Parser(object):

    def __init__(self, dir):
        """
        :param dir: directory root
        """
        if not os.path.exists(dir):
            print("Directory <{}> doesn't exist".format(dir),file=sys.stderr)
            sys.exit()
        self._dir = dir
        self._documents = []



    def load(self, load_content=True, extensions=("pdf","txt",'excel','excelx')):
        """
        Load data recursively from root directory
        :return: None
        """
        for base, dirs, files in os.walk(self._dir):
            for f in files:
                if Parser.get_extension(f) in extensions:
                    doc = Document(path=base, title=f)
                    if load_content:
                         file_opened = open(doc.full_path, encoding=ENCODING)
                         doc._content = file_opened.read()
                         file_opened.close()
                    self._documents.append(doc)


    @staticmethod
    def create_dir(dir_path):
        """
        :param dir_path:
        :return:
        """
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
            return True
        return False

    @staticmethod
    def remove_dir(dir_path):
        """
        :param dir_path:
        :return:
        """
        if os.path.exists(dir_path):
            rmtree(dir_path)
            return True
        return False

    @staticmethod
    def write_file(file_path,text):
        """
        :param file_path:
        :param text:
        :return:
        """
        with open(file_path, 'w', encoding=ENCODING) as out:
            out.write(text)



    @staticmethod
    def get_extension(file):
        """
        :param file:
        :param ext:
        :return:
        """
        return str(file).split(".")[-1]

    @staticmethod
    def read_excelfile(file_path):
        """
        :param file:
        :return:
        """
        if not os.path.exists(file_path):
            raise NameError("Parser.read_excelfile: File <{}> doesn't exist ".format(file_path))
        if Parser.get_extension(file_path) not in ('xls', 'xlsx'):
            raise NameError('Parser.read_excelfile: Extension is not xls, xlsx in file <{}>'.format(file_path))

        text = ""
        book = xlrd.open_workbook(file_path, logfile=open("log_file_xlrd.txt", "w"))
        for sheet_name in book.sheet_names():
            sheet = book.sheet_by_name(sheet_name)
            for row in range(0, sheet.nrows):
                for word in sheet.row_values(row):
                    if len(str(word).strip()) != 0:
                        text +=str(word).strip()+" "

        return text
