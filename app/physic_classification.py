import os
import sys
import time
from shutil import copyfile
from concurrent.futures import ThreadPoolExecutor, as_completed
from parser import Parser
from extra_decorators import singleton
import settings

@singleton
class Physic_classification(object):

    def __init__(self, data, src, output_dir=settings.CORPUS_CLASSIFIED_PATH):
        """
        :param data: 
        :param src: 
        :param output_dir: 
        """
        if not os.path.exists(src):
            print("Source file <{}> doesn't exist".format(src), file=sys.stderr)

        # init
        self._time = 0
        self._data = data
        self._src = src
        self._output_dir=output_dir
        #remove dir
        Parser.remove_dir(self._output_dir)
        # create output dir
        Parser.create_dir(self._output_dir)




    def copy_files_worker(self,cluster_name, classified_files):
        """
        :param cluster_name: 
        :param files: 
        :return: 
        """
        path_cluster = "{}/{}".format(self._output_dir,cluster_name)
        Parser.create_dir(path_cluster)

        for base, dirs, files in os.walk(self._src):
            for file in files:
                if file in classified_files:
                    src = "{}/{}".format(base,file)
                    if  os.path.exists(src):
                        dst = "{}/{}".format(path_cluster,file)
                        copyfile(src, dst)
                    else:
                        print("File <{}> doesn't exist".format(src), file=sys.stderr)
        return cluster_name



    def start_parallel_physic_classification(self, verbose=True):
        """
        :param verbose: 
        :return: 
        """
        futures = []
        start = time.time()
        with ThreadPoolExecutor(max_workers=os.cpu_count()) as e:

            for key, value in self._data.items():
                # lambda function get key by value dictionnaire
                reverse_dict_extensions = lambda file_ext: str([k for k,v in settings.CONVERT_EXTENSIONS_DICT.items() if v==file_ext][0])
                # get files classied with the right extension
                files_classified = list(map(lambda file: file.replace(".{}".format(Parser.get_extension(file)),".{}".format(reverse_dict_extensions(Parser.get_extension(file)))),value[1]))
                # set cluster name index cluester
                cluster_name = "{}".format(key)
                # submit worker thread
                future = e.submit(self.copy_files_worker, cluster_name, files_classified)
                # append thread or future
                futures.append(future)

            if verbose:
                for f in as_completed(futures):
                    print(f.result())

        end = time.time()
        self._time = end-start




