# coding: utf8
import nltk
import re
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
import pandas as pd
from extra_decorators import singleton
from settings import ENCODING

@singleton
class Corpus(object):
    def __init__(self, documents, lang='french' ):
        """
        :param lang: 
        :param documents: 
        """

        self._lang = lang
        self._stemmer = SnowballStemmer(lang)
        self._documents = documents

        # total vocab stemmed and tokenized
        self._total_vocab_stemmed = []
        self._total_vocab_tokenized = []



    @property
    def stopwords(self):
        """
        :return: 
        """
        return stopwords.words(self._lang)

    @property
    def total_vocab_stemmed(self):
        """
        :return: 
        """
        if len(self._total_vocab_stemmed) == 0:
            for doc in self._documents:
                self._total_vocab_stemmed.extend(self.tokenize_and_stem(doc._content))

        return self._total_vocab_stemmed

    @property
    def total_vocab_tokenized(self):
        """
        :return: 
        """
        if len(self._total_vocab_tokenized) == 0:
            for doc in self._documents:
                self._total_vocab_tokenized.extend(self.tokenize_only(doc._content))

        return self._total_vocab_tokenized


    @property
    def vocab_frame(self):
        """
        :return: 
        """
        vocab_frame = pd.DataFrame({'words': self._total_vocab_tokenized}, index=self._total_vocab_stemmed)
        return vocab_frame

    def tokenize_and_stem(self, text):
        """
        :return: tokenize and return stem
        """
        # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
        tokens = [word for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
        filtered_tokens = []
        # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
        for token in tokens:
            if re.search('[a-zA-Z]', token):
                filtered_tokens.append(token)
        stems = [self._stemmer.stem(t) for t in filtered_tokens]
        return stems

    def tokenize_only(self, text):
        """
        :return: filtered tockens
        """
        # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
        tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
        filtered_tokens = []
        # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
        for token in tokens:
            if re.search('[a-zA-Z]', token):
                filtered_tokens.append(token)
        return filtered_tokens



