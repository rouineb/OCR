from settings import ENCODING

def singleton(myclass):
    """
    :type Decorator of singleton pattern
    :param myclass:
    :return: instance of class
    """
    instances={}
    def getInstance(*args,**kwargs):
        """
        :param args:
        :param kwargs:
        :return:
        """
        if myclass not in instances:
            instances[myclass]=myclass(*args,**kwargs)

        return instances[myclass]
    myinstance = getInstance
    # return myclass if static function is clalled by classname
    return myclass if len(instances) == 0 else myinstance




